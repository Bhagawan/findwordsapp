package com.example.wordscubeapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import com.example.wordscubeapp.view.menu.MenuFragment
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MenuFragment())
                .commitNow()
        }
        onBackPressedDispatcher.addCallback(this , object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(supportFragmentManager.backStackEntryCount >= 1) supportFragmentManager.popBackStack()
                else finish()
            }
        })
        setBackground()
    }

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { findViewById<FrameLayout>(R.id.container).background = BitmapDrawable(resources, it) }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
        }
        Picasso.get().load("http://195.201.125.8/FindWordsApp/back.png").into(target)
    }
}