package com.example.wordscubeapp.view.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wordscubeapp.R
import com.example.wordscubeapp.util.LetterCoord
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class Game(context: Context, attributeSet: AttributeSet): View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private val field = Array(5) {Array(5){' '} }
    private val letters = arrayOf('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я')
    private val probability = arrayOf(0.07998f, 0.0959f, 0.14123f, 0.1581f, 0.18787f, 0.2727f, 0.27283f, 0.28223f, 0.29864f, 0.37231f, 0.38439f, 0.41925f, 0.46268f, 0.49471f, 0.56171f, 0.67154f, 0.69958f, 0.74704f, 0.80177f, 0.86495f, 0.8911f, 0.89377f, 0.90343f, 0.90829f, 0.92279f, 0.92997f, 0.93358f, 0.93395f, 0.95293f, 0.97028f, 0.97359f, 0.97998f, 0.99999f)
    private val words = context.resources.openRawResource(R.raw.words).bufferedReader().readLines()

    val firstPlayerPoints = MutableLiveData(0)
    val secondPlayerPoints = MutableLiveData(0)
    private val mutableNotification = MutableLiveData<String>()
    val notification :LiveData<String> = mutableNotification


    private val currentPick = ArrayList<LetterCoord>()
    private var currentPlayer = true  // true - first   false - second
    private val firstPlayerWords = ArrayList<String>()
    private val secondPlayerWords = ArrayList<String>()

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
        generateField()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                val c = findLetter(event.x, event.y)
                if(!currentPick.contains(c)) currentPick.add(c)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                val c = findLetter(event.x, event.y)
                val d = (currentPick[currentPick.size - 1].row - c.row).absoluteValue + (currentPick[currentPick.size - 1].column - c.column).absoluteValue
                if(!currentPick.contains(c) && d == 1) currentPick.add(c)
                return true
            }
            MotionEvent.ACTION_UP -> {
                checkWord()
                currentPick.clear()
                currentPlayer = !currentPlayer
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    private fun drawField(c: Canvas) {
        val size = mWidth * 0.2f
        val p = Paint()
        p.color = Color.WHITE
        p.style = Paint.Style.STROKE
        p.strokeWidth = 5.0f
        p.textSize = size * 0.75f
        p.textAlign = Paint.Align.CENTER
        for(row in 0 until 5) {
            for(column in 0 until 5) {
                if(currentPick.contains(LetterCoord(row, column))) p.color = Color.YELLOW else p.color = Color.WHITE
                c.drawRect(column * size, row * size, (column + 1) * size, (row + 1) * size, p)
                c.drawText(field[row][column].toString(), (column + 0.5f) * size, (row + 0.75f) * size, p)
            }
        }
    }

    fun restart() {
        generateField()
        firstPlayerWords.clear()
        secondPlayerWords.clear()
        firstPlayerPoints.postValue(0)
        secondPlayerPoints.postValue(0)
    }

    fun getFirstPlayerHistory() : List<String> = firstPlayerWords

    fun getSecondPlayerHistory() : List<String> = secondPlayerWords

    private fun generateField() {
        for(row in 0 until 5) {
            for(column in 0 until 5) {
                field[row][column] = getChar()
            }
        }
    }

    private fun getChar() : Char {
        val r = Random.nextFloat()
        for(n in letters.indices) {
            if(r in (if(n != 0) probability[n - 1] else 0f)..probability[n]) {
                return letters[n]
            }
        }
        return 'О'
    }

    private fun findLetter(x: Float, y: Float) : LetterCoord {
        val size = mWidth * 0.2f
        return LetterCoord((y / size).toInt().coerceAtLeast(0).coerceAtMost(4), (x / size).toInt().coerceAtLeast(0).coerceAtMost(4))
    }

    private fun checkWord() {
        var word = ""
        for(n in currentPick) word += field[n.row][n.column]
        word = word.lowercase()
        if(words.contains(word) && !firstPlayerWords.contains(word) && !secondPlayerWords.contains(word)) {
            mutableNotification.postValue(word)
            if(currentPlayer) {
                firstPlayerWords.add(word)
                firstPlayerPoints.postValue((firstPlayerPoints.value ?: 0) + 1)
            }
            else {
                secondPlayerWords.add(word)
                secondPlayerPoints.postValue((secondPlayerPoints.value ?: 0) + 1)
            }
        }
    }
}