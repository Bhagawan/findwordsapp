package com.example.wordscubeapp.view.game

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordscubeapp.R
import com.example.wordscubeapp.databinding.FragmentGameBinding
import com.example.wordscubeapp.databinding.PopupHistoryBinding
import com.example.wordscubeapp.databinding.PopupWordBinding
import com.example.wordscubeapp.util.HistoryAdapter

class GameFragment : Fragment() {
    private lateinit var binding :FragmentGameBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGameBinding.inflate(layoutInflater)

        binding.btnGameBack.setOnClickListener { parentFragmentManager.popBackStack() }
        binding.btnGameRestart.setOnClickListener { binding.game.restart() }
        binding.game.firstPlayerPoints.observe(viewLifecycleOwner) {
            binding.textScoreOne.text = it.toString()
            binding.layoutPlayerOne.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_score, null)
            binding.layoutPlayerTwo.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_score_active, null)
        }
        binding.game.secondPlayerPoints.observe(viewLifecycleOwner) {
            binding.textScoreTwo.text = it.toString()
            binding.layoutPlayerOne.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_score_active, null)
            binding.layoutPlayerTwo.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_score, null)
        }
        binding.btnGameHistory.setOnClickListener { showHistory() }
        binding.game.notification.observe(viewLifecycleOwner) { showNotification(it.uppercase()) }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showNotification(word: String) {
        val popupBinding: PopupWordBinding = PopupWordBinding.inflate(layoutInflater)

        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        popupBinding.testPopupWord.text = word

        popupBinding.root.setOnClickListener { popupWindow.dismiss() }

        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showHistory() {
        val popupBinding: PopupHistoryBinding = PopupHistoryBinding.inflate(layoutInflater)

        val width = (binding.root.width * 0.9f).toInt()
        val height = (binding.root.height * 0.9f).toInt()

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        popupBinding.recyclerHistoryFirst.layoutManager = LinearLayoutManager(context)
        popupBinding.recyclerHistoryFirst.adapter = HistoryAdapter(binding.game.getFirstPlayerHistory())
        popupBinding.recyclerHistorySecond.layoutManager = LinearLayoutManager(context)
        popupBinding.recyclerHistorySecond.adapter = HistoryAdapter(binding.game.getSecondPlayerHistory())

        popupBinding.btnHistoryClose.setOnClickListener { popupWindow.dismiss() }

        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }
}