package com.example.wordscubeapp.view.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.wordscubeapp.R
import com.example.wordscubeapp.databinding.FragmentMenuBinding
import com.example.wordscubeapp.view.game.GameFragment

class MenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMenuBinding.inflate(layoutInflater)
        binding.button.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.container, GameFragment()).addToBackStack("")
                .setCustomAnimations(R.anim.anim_in, R.anim.anim_out)
                .commit()
        }

        return binding.root
    }

}