package com.example.wordscubeapp.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordscubeapp.databinding.ItemHistoryBinding

class HistoryAdapter(private val words: List<String>): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(words[position])
    }

    override fun getItemCount(): Int = words.size

    inner class ViewHolder(private val binding : ItemHistoryBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(word: String) {
            binding.testItemHistoryWord.text = word
        }
    }
}