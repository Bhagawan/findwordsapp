package com.example.wordscubeapp.util

data class LetterCoord(val row: Int, val column: Int) {

    override fun equals(other: Any?): Boolean {
        if(other is LetterCoord) {
            return other.column == column && other.row == row
        }
        return false
    }

    override fun hashCode(): Int {
        var result = row
        result = 31 * result + column
        return result
    }
}
