package com.example.wordscubeapp.util

import androidx.annotation.Keep

@Keep
data class WordsSplashResponse(val url : String)